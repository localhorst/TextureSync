#!/bin/bash

# use the correct working directory
cd $(dirname "$0")

# compile java code
mkdir -p bin
javac -sourcepath src -d bin src/creation/Creator.java

# recreate output dir
rm -rf outputdir
mkdir -p outputdir/data/textures

# create textures
java -classpath bin creation.Creator

# place a server binary in the output directory
(
  cd ../server/texture-sync-server
  cargo build --release
)
cp ../server/texture-sync-server/target/release/texture-sync-server ./outputdir/
