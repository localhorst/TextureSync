package creation;

import java.awt.Color;

public class Dictionary {
	public static final String[] adjectives = { "fast", "slow", "long", "short", "fat", "big", "small", "angry",
			"awful", "calm", "clever", "crazy", "dirty", "excited", "evil", "kind", "lucky", "poor", "shy", "soft",
			"tall", };
	public static final String[] animals = { "bat", "bee", "camel", "cat", "chicken", "cod", "deer", "dog", "duck",
			"fly", "fox", "frog", "horse", "koala", "lion", "mouse", "owl", "pig", "rabbit", "rat", "tiger", "turtle",
			"wolf", "zebra" };
	public static final NamedColor[] colors = { new NamedColor("red", Color.RED),
			new NamedColor("orange", Color.ORANGE), new NamedColor("yellow", Color.YELLOW),
			new NamedColor("green", Color.GREEN), new NamedColor("blue", Color.BLUE),
			new NamedColor("magenta", Color.MAGENTA), new NamedColor("black", Color.BLACK), };
}
