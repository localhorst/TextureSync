package creation;

import java.util.ArrayList;
import java.util.Random;

public class NameCreator {
	private static void shuffle(ArrayList<String> list, Random r) {
		for (int low=0; low<list.size()-1; low++) {
			int high = r.nextInt(list.size() - low) + low;
			
			// swap
			String sLow = list.get(low);
			String sHigh = list.get(high);
			list.set(low, sHigh);
			list.set(high, sLow);
		}
	}

	public static String[] generateNames(Random r) {
		ArrayList<String> names = new ArrayList<>(); 
		for (String ending : new String[] {"jpg", "png"}) {
			for (String animal : Dictionary.animals) {
				for (String adjective : Dictionary.adjectives) {
					String name = adjective + "-" + animal + "." + ending;
					names.add(name);
				}
			}
		}
		shuffle(names, r);
		while (names.size() > 1000) {
			names.remove(names.size() - 1);
		}
		return names.toArray(new String[1000]);
	}
}
