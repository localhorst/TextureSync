package creation;

import java.awt.Color;

public class NamedColor {
	public final String name;
	public final Color color;

	public NamedColor(String name, Color color) {
		this.name = name;
		this.color = color;
	}
}
