package org.hso.texturesyncclient.controller.net

// These types will be converted to the model.DataModel

import org.hso.texturesyncclient.model.Sha256
import org.hso.texturesyncclient.model.Texture
import org.hso.texturesyncclient.model.TextureFormat
import java.lang.Exception
import java.util.*

@Suppress("ArrayInDataClass")
internal data class InternalTexture(
    val id: String,
    val name: String,
    val tags: Array<String>,
    val format: String,
    val resolution: Array<Int>,
    val added_on: Array<Int>,
    val texture_hash: String
) {

    constructor(tex: Texture) : this(
        id = tex.id.toString(),
        name = tex.name,
        tags = tex.tags.clone(),
        format = when (tex.format) {
            TextureFormat.PNG -> "png"
            TextureFormat.JPEG -> "jpeg"
        },
        resolution = arrayOf(tex.resolution.first, tex.resolution.second),
        added_on = arrayOf(
            tex.addedOn.get(Calendar.YEAR), //
            tex.addedOn.get(Calendar.MONTH) + 1, //
            tex.addedOn.get(Calendar.DAY_OF_MONTH)
        ),
        texture_hash = tex.textureHash.toString()
    )

    @Throws(ConnectionException::class)
    fun toTexture(): Texture {
        try {
            return Texture(
                id = UUID.fromString(id),
                name = name,
                tags = tags.clone(),
                format = when (format.toLowerCase()) {
                    "jpeg" -> TextureFormat.JPEG
                    "jpg" -> TextureFormat.JPEG
                    "png" -> TextureFormat.PNG
                    else -> throw ConnectionInvalidJsonException()
                },
                resolution = Pair(resolution[0], resolution[1]),
                addedOn = GregorianCalendar(added_on[0], added_on[1] - 1, added_on[2]),
                textureHash = Sha256(texture_hash)
            )
        } catch (e: Exception) { // i Know, but no time :[]
            throw ConnectionInvalidJsonException()
        }
    }
}