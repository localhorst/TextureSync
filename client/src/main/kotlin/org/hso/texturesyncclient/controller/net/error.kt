@file:Suppress("MemberVisibilityCanBePrivate")

package org.hso.texturesyncclient.controller.net

import java.lang.Exception

sealed class ConnectionException(override val message: String) : Exception(message)

class ConnectionErrorException(errorCode: Int, errorMessage: String) :
    ConnectionException("$errorCode $errorMessage") {
    internal constructor(err: ErrorPackage) : this(err.code, err.message)
}

class ConnectionUnexpectedPacketException : ConnectionException("Got Unexpected Type of Packet")
class ConnectionInvalidJsonException : ConnectionException("The Format of the Json Received is Unexpected.")


sealed class PacketException(msg: String) : ConnectionException(msg)
class PacketTooLongException : PacketException("The Package is too long.")
class PacketInvalidType : PacketException("The Package has an Invalid Type.")
class PacketInvalidData : PacketException("The Package has an Invalid Data. (e.g. Invalid Json.)")
