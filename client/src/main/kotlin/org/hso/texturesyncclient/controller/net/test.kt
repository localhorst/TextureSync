package org.hso.texturesyncclient.controller.net

import java.net.*

fun main() {
    // Just some test code.

    val con = Connection(InetAddress.getByName("::1"))

    con.ping()


    println("Query:")
    for (tex in con.query(
        arrayOf("Red", "Food")
    )) {
        println(tex.toString())
    }
    println()

    con.close()
}