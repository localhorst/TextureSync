package org.hso.texturesyncclient.view.mainView

import javafx.geometry.Insets
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.CornerRadii
import javafx.scene.paint.Paint
import tornadofx.*

class FolderView : View("FolderView"){

    override val root = flowpane {
        hgap = 5.0
        vgap = 5.0
        paddingAll = 10.0
        background = Background(BackgroundFill(Paint.valueOf("#2b2b2b"), CornerRadii.EMPTY, Insets.EMPTY))
    }

}