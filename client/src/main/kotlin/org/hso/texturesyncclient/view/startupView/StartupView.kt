package org.hso.texturesyncclient.view.startupView

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXSpinner
import com.jfoenix.controls.JFXTextField
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.input.KeyCode
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.CornerRadii
import javafx.scene.paint.Paint
import tornadofx.*

class StartupView : View("TextureSync") {

    val labelStatus = Label("Verbindung zum Server einrichten")
    val spinnerStatus = JFXSpinner()
    val tfServerIP = JFXTextField()
    val btnConnect = JFXButton("Manuell Verbinden")

    private val svc: StartupViewController by inject()


    override val root = borderpane {
        minWidth = 1000.0
        minHeight = 500.0
        background = Background(BackgroundFill(Paint.valueOf("#2b2b2b"), CornerRadii.EMPTY, Insets.EMPTY))

        center = vbox(50) {
            maxWidth = 350.0
            alignment = Pos.CENTER

            add(labelStatus)
            add(spinnerStatus)

            vbox(10) {
                alignment = Pos.CENTER

                label("Server-Adresse") {
                    style = "-fx-font: 15px Verdana; -fx-text-fill: #2b7bbb;"
                }
                add(tfServerIP)
                add(btnConnect)
            }

        }

        style {
            spinnerStatus.isVisible = false

            labelStatus.style = "-fx-font: 20px Verdana; -fx-text-fill: #2b7bbb;"

            btnConnect.style = "-fx-button-type: RAISED; -fx-background-color: #3c3f41; -fx-text-fill: #2b7bbb;"

            //tfServerIP.style = "-fx-text-fill: #b15b2e;"
            tfServerIP.style {
                textFill = Paint.valueOf("#b15b2e")
                alignment = Pos.BASELINE_CENTER
            }

        }

        btnConnect.setOnAction {
            svc.btnConnectAction(tfServerIP.text)
        }

        tfServerIP.setOnKeyPressed {
            if (it.code == KeyCode.ENTER) {
                svc.btnConnectAction(tfServerIP.text)
            }
        }

    }
}