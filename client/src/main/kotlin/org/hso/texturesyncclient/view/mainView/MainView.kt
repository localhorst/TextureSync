package org.hso.texturesyncclient.view.mainView

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXChipView
import javafx.geometry.Insets

import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.CornerRadii
import javafx.scene.paint.Paint
import tornadofx.*

class MainView : View("TextureSync") {

    val cvSearch = JFXChipView<String>()
    private val btnImport = JFXButton("+")
    val folderView = find(FolderView::class)
    val detailView = find(DetailView::class)

    private val mvc: MainViewController by inject()

    fun repeatSearch() {
        mvc.cvSearchAction(cvSearch.chips)
    }

    override val root = anchorpane {

        background = Background(BackgroundFill(Paint.valueOf("#2b2b2b"), CornerRadii.EMPTY, Insets.EMPTY))
        prefWidth = FX.primaryStage.width
        prefHeight = FX.primaryStage.height

        borderpane {
            right = detailView.root
            center = vbox {
                add(cvSearch)
                scrollpane {
                    style = "-fx-background-color:transparent;"
                    isFitToWidth = true
                    isFitToHeight = true

                    add(folderView.root)
                }
            }

            anchorpaneConstraints {
                topAnchor = 0
                bottomAnchor = 0
                rightAnchor = 0
                leftAnchor = 0
            }
        }

        add(btnImport)

        style {
            cvSearch.promptText = "Suche"
            cvSearch.paddingAll = 5.0
            cvSearch.minHeight = 65.0
            cvSearch.style = "-fx-background-color: #53585b; -fx-text-inner-color: #b15b2e;"

            btnImport.buttonType = JFXButton.ButtonType.RAISED
            btnImport.styleClass.add("jfx-floating-action-button")
            btnImport.anchorpaneConstraints {
                bottomAnchor = 5
                rightAnchor = 5
            }
        }

        // actions
        cvSearch.chips.onChange {
            mvc.cvSearchAction(cvSearch.chips)
        }

        btnImport.setOnAction {
            mvc.btnImportAction()
        }

        // TODO: on chipview update on name update

        detailView.cvTags.chips.onChange {
            detailView.btnSubmit.isVisible = true
        }

        detailView.nameInfo.textProperty().onChange {
            detailView.btnSubmit.isVisible = true
        }

        detailView.btnSubmit.setOnAction {
            mvc.updateTags()
            detailView.btnSubmit.isVisible = false
        }

        //keyboard actions
        shortcut("Ctrl+I") {
            mvc.btnImportAction()
        }

        shortcut("Ctrl+E") {
            mvc.scExport()
        }
    }

}