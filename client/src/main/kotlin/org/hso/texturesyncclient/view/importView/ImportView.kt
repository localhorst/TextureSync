package org.hso.texturesyncclient.view.importView

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXChipView
import com.jfoenix.controls.JFXTextField
import javafx.geometry.Pos
import javafx.scene.layout.Background
import javafx.geometry.Insets
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.CornerRadii
import javafx.scene.paint.Paint
import javafx.scene.layout.Priority
import org.hso.texturesyncclient.view.mainView.Preview3D
import tornadofx.*

class ImportView : View("TextureSync") {

    val tfFilePath = JFXTextField()
    val tfName = JFXTextField()
    val cvTags = JFXChipView<String>()
    val btnImport = JFXButton("Importieren")

    val preview = Preview3D()

    private val btnBack = JFXButton("Zurück")

    private val ivc: ImportViewController by inject()

    init {
        btnImport.isVisible = false
        preview.root.isVisible = false
    }

    override val root = borderpane {
        prefWidth = FX.primaryStage.width
        prefHeight = FX.primaryStage.height
        background = Background(BackgroundFill(Paint.valueOf("#2b2b2b"), CornerRadii.EMPTY, Insets.EMPTY))

        center = vbox(50) {
            maxWidth = 350.0
            alignment = Pos.CENTER

            label("Textur hinzufügen") {
                style = "-fx-font: 20px Verdana; -fx-text-fill: #2b7bbb;"
            }

            add(preview)

            vbox(20) {
                hbox(10) {
                    add(tfFilePath)
                    button("Datei öffnen") {
                        style = "-fx-button-type: RAISED; -fx-background-color: #3c3f41; -fx-text-fill: #2b7bbb;"

                        setOnAction {
                            ivc.btnFileChooserAction()
                            ivc.validateImport()
                        }
                    }
                }

                vbox(5) {
                    label("Name") {
                        style = "-fx-font: 14px Verdana; -fx-text-fill: #2b7bbb;"
                    }
                    add(tfName)
                }

                vbox(5) {
                    label("Tags") {
                        style = "-fx-font: 14px Verdana; -fx-text-fill: #2b7bbb;"
                    }
                    add(cvTags)
                }

                vbox(5) {
                    alignment = Pos.CENTER
                    add(btnImport)
                    add(btnBack)
                }
            }
        }

        style {
            tfFilePath.style = "-fx-text-fill: #b15b2e;"
            tfFilePath.promptText = "Pfad zur Datei"
            tfFilePath.hgrow = Priority.ALWAYS

            tfName.style = "-fx-text-fill: #b15b2e;"
            tfName.promptText = "Name eingeben"

            cvTags.style = "-fx-background-color: #53585b; -fx-text-inner-color: #b15b2e;"
            //TODO change color of Chip´s see: https://github.com/jfoenixadmin/JFoenix/blob/master/jfoenix/src/main/resources/com/jfoenix/assets/css/controls/jfx-chip-view.css#L52

            btnImport.style = "-fx-button-type: RAISED; -fx-background-color: #b15b2e; -fx-text-fill: #3c3f41; -fx-font-size: 15;"
            btnBack.style = "-fx-button-type: RAISED; -fx-background-color: #3c3f41; -fx-text-fill: #2b7bbb; -fx-padding: 10;"
        }

        tfFilePath.textProperty().addListener{ _, _, _ -> ivc.validateImport() }
        tfName.textProperty().addListener{ _, _, _ -> ivc.validateImport() }

        cvTags.chips.onChange {
            ivc.validateImport()
        }

        btnImport.setOnAction {
            ivc.btnImportAction()
        }

        btnBack.setOnAction {
            ivc.btnBackAction()
        }

    }

}