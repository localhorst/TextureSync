package org.hso.texturesyncclient.view.mainView

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXChipView
import com.jfoenix.controls.JFXTextField
import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.scene.image.Image
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.CornerRadii
import javafx.scene.paint.Paint
import tornadofx.*

class DetailView : View() {

    val preview = Preview3D()
    val cvTags = JFXChipView<String>()

    val nameInfo = JFXTextField().addClass("metadata")
    val resolutionInfo = label().addClass("metadata")
    val formatInfo = label().addClass("metadata")
    val dateInfo = label().addClass("metadata")

    val btnSubmit = JFXButton("Ändern").addClass("btn-blue")

    val metadataPanel = gridpane {
        row {
            label("Name ").addClass("metadata")
            add(nameInfo)
        }
        row {
            label("Auflösung ").addClass("metadata")
            add(resolutionInfo)
        }
        row {
            label("Format ").addClass("metadata")
            add(formatInfo)
        }
        row {
            label("Einfügedatum ").addClass("metadata")
            add(dateInfo)
        }
    }

    override val root = form {
        minWidth = 250.0
        background = Background(BackgroundFill(Paint.valueOf("#3a3a3a"), CornerRadii.EMPTY, Insets.EMPTY))

        fieldset(labelPosition = Orientation.VERTICAL) {

            field {
                vbox(7) {
                    add(preview)
                }
            }

            field {
                add(metadataPanel)
            }

            field {
                minHeight = 155.0
                add(cvTags)
            }

            field {
                add(btnSubmit)
            }

        }
    }

    init {
        // set a default texture
        preview.setTexture(Image("icons/TextureSync_Icon_256x256.jpeg"))
        btnSubmit.useMaxWidth = true

    }
}