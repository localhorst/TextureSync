package org.hso.texturesyncclient.view.startupView

import org.hso.texturesyncclient.controller.RootController
import tornadofx.Controller


class StartupViewController : Controller() {

    private val sv = find(StartupView::class)
    private val rootc = find(RootController::class)

    fun initConnection() {
        println("init StartupViewController")
        startConnectionUI()
        runAsync {
            rootc.initConnection(" ")
        } ui {
            // reset for later use
            endConnectionUI()
        }
    }

    fun setServerAddress(address: String) {
        //sv.tfServerIP.text = address
        sv.tfServerIP.isFocusTraversable = false
    }

    fun btnConnectAction(name: String) {
        startConnectionUI()
        runAsync {
            rootc.initConnection(name)
        } ui {
            // reset for later use
            endConnectionUI()
        }
    }

    /**
     * show spinner and block textfied + button and set label
     */
    private fun startConnectionUI() {
        sv.labelStatus.text = "Verbinden ..."
        sv.tfServerIP.isEditable = false
        sv.btnConnect.isDisable = true
        sv.spinnerStatus.isVisible = true
    }

    /**
     * remove spinner and unblock textfied + button and set label
     */
    private fun endConnectionUI() {
        sv.spinnerStatus.isVisible = false
        sv.labelStatus.text = "Verbindung zum Server einrichten"
        sv.tfServerIP.isEditable = true
        sv.btnConnect.isDisable = false
        sv.tfServerIP.clear()
    }


}


