package org.hso.texturesyncclient.view.mainView

import javafx.collections.ObservableList
import javafx.scene.image.Image
import org.hso.texturesyncclient.controller.RootController
import org.hso.texturesyncclient.model.GUIModel
import org.hso.texturesyncclient.model.Texture
import tornadofx.Controller

class MainViewController : Controller() {

    private val mv = find(MainView::class)
    private val rootc = find(RootController::class)

    // FolderView elements
    private val folderView = mv.folderView.root

    // DetailView elements
    private val preview = mv.detailView.preview
    private val cvTags = mv.detailView.cvTags
    private var lockUpdate: Boolean = false //lock update func when the system changes the detailview chipview

    // FolderView functions
    fun addElement(element: GUIModel) {
        folderView.children.add(element)
    }

    // DetailView functions
    fun setPreview3DTexture(img: Image) {
        preview.setTexture(img)
    }

    fun setMeta(name: String, res: String, format: String, date: String) {
        with(mv.detailView) {
            nameInfo.text = name
            formatInfo.text = format
            resolutionInfo.text = res
            dateInfo.text = date
        }
    }

    fun setTags(chips: ObservableList<String>) {
        lockUpdate = false //dont trigger update with onChange
        cvTags.chips.clear()
        cvTags.chips.addAll(chips)
        lockUpdate = true //allow update with onChange
    }

    fun getTags(): ObservableList<String> {
        return cvTags.chips
    }

    fun updateTags() {
        if (lockUpdate) { //the chipView was changed by the user
            println("Tags changed")
            rootc.updateTexture(
                tags = cvTags.chips.toTypedArray(),
                name = mv.detailView.nameInfo.text
            )
        }
    }

    // MainView actions

    fun cvSearchAction(tags: ObservableList<String>) {
        // show spinner, block ui
        folderView.children.clear()
        mv.cvSearch.isDisable = true
        setPreview3DTexture(Image("icons/TextureSync_Icon_256x256.jpeg"))   // reset the 3DPreview to the logo
        RootController.selectedTexture = null

        runAsync {
            rootc.queryElements(tags)
        } ui {
            // when search finished
            mv.cvSearch.isDisable = false
        }
    }

    fun btnImportAction() {
        RootController.switchMainToImport()
    }

    fun removeTextureFromView(data: Texture) {
        // stream all children nodes, filter them as GUIModel with data.id == data.id, for any found object if it's still present remove it from the folderView
        folderView.children.stream().filter { x -> (x as GUIModel).data.id == data.id }.findAny()
            .ifPresent { x -> folderView.children.remove(x) }
    }

    fun setVisibleMetaTags(bool: Boolean) {
        if (bool) {
            mv.detailView.metadataPanel.isVisible = true
            mv.detailView.btnSubmit.isVisible = false
            cvTags.isVisible = true
        } else {
            mv.detailView.metadataPanel.isVisible = false
            mv.detailView.btnSubmit.isVisible = false
            cvTags.isVisible = false
        }
    }

    fun scExport() {
        RootController.selectedTexture?.let { rootc.exportTexture(RootController.selectedTexture!!) }
    }

}