package org.hso.texturesyncclient.model

import org.hso.texturesyncclient.controller.RootController
import tornadofx.Controller

class GUIModelController : Controller() {

    private val rootc = find(RootController::class)

    lateinit var lastSelected: GUIModel
    fun isLastSelectedInitialized() = ::lastSelected.isInitialized

    fun export(data: Texture) {
        rootc.exportTexture(data)
    }

    fun delete() {
        rootc.deleteTexture()
    }

    fun previewSelectedAction(data: Texture) {
        rootc.showDetail(data)
    }

    fun setSelected(model: GUIModel) {
        rootc.setSelectedTexture(model)
    }
}