package org.hso.texturesyncclient.app

import javafx.scene.image.Image
import javafx.stage.Stage
import org.hso.texturesyncclient.controller.SettingsController
import org.hso.texturesyncclient.view.startupView.StartupView
import org.hso.texturesyncclient.view.startupView.StartupViewController
import tornadofx.App

class Main : App(StartupView::class) {

    private val svc: StartupViewController by inject()

    override fun start(stage: Stage) {

        // Calling super.start early prevents a weird layouting bug
        // were the button of the window is white.
        // This could(?) be caused by stage and StartupView having
        // a different min-size.
        super.start(stage)

        stage.minWidth = 1050.00
        stage.minHeight = 700.00
        stage.width = 1050.00
        stage.height = 700.00
        stage.isResizable = true
        stage.icons.add(Image("icons/TextureSync_Icon_256x256.png"))
        stage.setOnCloseRequest { System.exit(0) }

        stage.scene.stylesheets.add("/css/Styles.css")

        SettingsController.init()
        svc.initConnection()
    }

}