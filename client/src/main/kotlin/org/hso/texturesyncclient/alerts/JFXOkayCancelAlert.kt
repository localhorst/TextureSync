package org.hso.texturesyncclient.alerts

import com.jfoenix.controls.JFXAlert
import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXDialogLayout

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.text.Text
import tornadofx.FX

/**
 * @param heading   Heading text of the alert
 * @param body      Content text of the alert
 * @param btnStyle  Style of the buttons
 */
class JFXOkayCancelAlert(heading: String, body: String, private var btnStyle: String) {

    var okayAction: EventHandler<ActionEvent>? = null
    var cancelAction: EventHandler<ActionEvent>? = null

    private var okayText = "Löschen"
    private var cancelText = "Abbrechen"
    private var headingText = Text(heading)
    private var bodyText = Text(body)

    init {
        //headingText.style = "-fx-font: 14px Verdana; -fx-text-fill: #2b7bbb;"
        //bodyText.style = "-fx-font: 14px Verdana; -fx-text-fill: #2b7bbb;"
    }

    fun showAndWait() {
        val alert = JFXAlert<Void>(FX.primaryStage)

        val okayBtn = JFXButton(okayText)
        okayBtn.addEventHandler(ActionEvent.ACTION) { alert.close() }
        okayBtn.addEventHandler(ActionEvent.ACTION, okayAction!!)
        okayBtn.buttonType = JFXButton.ButtonType.RAISED
        okayBtn.prefHeight = 32.0
        okayBtn.style = btnStyle

        val cancelBtn = JFXButton(cancelText)
        cancelBtn.addEventHandler(ActionEvent.ACTION) { alert.close() }
        cancelBtn.addEventHandler(ActionEvent.ACTION, cancelAction!!)
        cancelBtn.buttonType = JFXButton.ButtonType.RAISED
        cancelBtn.prefHeight = 32.0
        cancelBtn.style = btnStyle

        val content = JFXDialogLayout()
        //content.background = Background(BackgroundFill(Paint.valueOf("#2b2b2b"), CornerRadii.EMPTY, Insets.EMPTY))
        content.setActions(cancelBtn, okayBtn)
        content.setHeading(headingText)
        content.setBody(bodyText)
        alert.setContent(content)
        alert.showAndWait()
    }

}
