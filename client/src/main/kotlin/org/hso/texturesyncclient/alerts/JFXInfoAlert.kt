package org.hso.texturesyncclient.alerts

import com.jfoenix.controls.JFXDialogLayout
import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXAlert
import javafx.event.ActionEvent
import javafx.scene.text.Text
import tornadofx.FX


/**
 * Creates a new JFoenix Alert to show some information
 * @param heading    Heading text of the alert
 * @param body      Content text of the alert
 * @param btnStyle  Style of the okay button
 */
class JFXInfoAlert(heading: String, body: String, private var btnStyle: String) {

    private var headingText = Text(heading)
    private var bodyText = Text(body)

    fun showAndWait() {
        val alert = JFXAlert<Void>(FX.primaryStage)

        val button = JFXButton("Okay")
        button.addEventHandler(ActionEvent.ACTION) { alert.close() }
        button.buttonType = JFXButton.ButtonType.RAISED
        button.prefHeight = 32.0
        button.style = btnStyle

        val content = JFXDialogLayout()
        content.setActions(button)
        content.setHeading(headingText)
        content.setBody(bodyText)
        alert.setContent(content)
        alert.showAndWait()
    }

}