# TextureSync
##Seminar PM - Projekt 1

Many 3D designers work in a team on large projects with a huge number of textures. The
Textures are often distributed on the computers of project members and are difficult to search. This
leads to more time being spent on coordination.

We want to deliver a product that stores the textures centrally and makes them available for each employee.
that makes it easy to search. These can then see directly from previews which textures
are possible. The central administration on the server also allows backups of the
texture data can be enabled.




##Authors

- Lukas Fürderer
- Robin Willmann
- Jannik Seiler
- Hendrik Schutter

###Angewandte Informatik Sommersemester 2019

