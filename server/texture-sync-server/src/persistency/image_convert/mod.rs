use crate::model::*;
use ::image::*;

// TODO: Deside on ::image::FilterType;
// TODO: Think about size; Hard Code??

const RESIZE: u32 = 256;

pub fn generate_preview(input: &[u8], format: TextureFormat) -> ImageResult<Vec<u8>> {
    // Yes, this guesses the format :D
    // Also the resize function takes a maximum size and preservs the ratio.
    let img =
        ::image::load_from_memory(input)?.resize(RESIZE, RESIZE, ::image::FilterType::Nearest);

    let mut out: Vec<u8> = Vec::new();

    match format {
        TextureFormat::JPEG => img.write_to(&mut out, ImageOutputFormat::JPEG(85))?,
        TextureFormat::PNG => img.write_to(&mut out, ImageOutputFormat::PNG)?,
    }

    Ok(out)
}
