#![allow(unused_variables)]
#![allow(dead_code)]

use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, Debug, Deserialize, Serialize, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub enum TextureFormat {
    #[serde(rename = "png")]
    PNG,
    #[serde(rename = "jpeg")]
    JPEG,
}

impl TextureFormat {
    fn variants() -> &'static [TextureFormat] {
        &[TextureFormat::PNG, TextureFormat::JPEG]
    }
}

#[cfg(test)]
mod tests {
    // Lol, I thought we would need custom code, like for Sha256, but it works out of the box :D
    // Anyhow, left the Test in.

    use super::*;
    use serde_json;

    #[test]
    fn serialize() {
        let format = serde_json::to_string_pretty(&TextureFormat::PNG).unwrap();
        assert_eq!(&format, r#""png""#);

        let format = serde_json::to_string_pretty(&TextureFormat::JPEG).unwrap();
        assert_eq!(&format, r#""jpeg""#);
    }

    #[test]
    fn deserialize() {
        let format: TextureFormat = serde_json::from_str(r#""png""#).unwrap();
        assert_eq!(format, TextureFormat::PNG);

        let format: TextureFormat = serde_json::from_str(r#""jpeg""#).unwrap();
        assert_eq!(format, TextureFormat::JPEG);
    }

    #[test]
    fn fail_deserialize() {
        assert_eq!(
            serde_json::from_str::<'_, TextureFormat>(r#""notafmt""#).is_err(),
            true
        );

        // Format must be lowercase!
        assert_eq!(
            serde_json::from_str::<'_, TextureFormat>(r#""PNG""#).is_err(),
            true
        );
    }
}
