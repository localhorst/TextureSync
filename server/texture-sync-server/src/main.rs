#[macro_use]
extern crate serde;

extern crate serde_json;

extern crate image;
extern crate lovecraft;

extern crate sha2;

extern crate num_cpus;

pub mod model;
pub mod persistency;
pub mod protocol;
pub mod search;
use protocol::*;

mod server_state;
use server_state::*;

use std::path::*;

fn main() -> std::io::Result<()> {
    lovecraft::invoke();

    println!("\n\n\t=== TextureSync Server {} ===\t\n\n\n", env!("CARGO_PKG_VERSION"));

    let data_path = Path::new("./data");
    println!("loading files from {:?}", data_path);
    let server_state = ServerState::new(data_path)?;

    let network_conf = ProtocolConfig::default();

    println!(
        "listening on {} : {}",
        network_conf.listen_addr, network_conf.port
    );

    match self::protocol::start_autoconnect_server_async(&network_conf) {
        Ok(_) => println!("Started Autoconnect Server"),
        Err(e) => println!("Starting Autoconnect Server failed: {:?}", e),
    }
    self::protocol::listen_forever(server_state, &network_conf)?;

    Ok(())
}
