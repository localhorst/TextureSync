
#Statusbericht KW21 - TextureSync

Sehr geehrter Herr Nikolaropoulos,

hier der Statusbericht der vergangenen Woche TextureSync.

#Vergangene Arbeitswoche

Robin Willmann ergänzte den Server um einen Logger für Events.

#Aktuelle Informationen über das Projekt

* Website mit aktuellem Projektplan: https://planner.mosad.xyz/TextureSync.html
* Repository mit Code und Dokumenten: https://git.mosad.xyz/localhorst/TextureSync

Sollten Sie Fragen haben, können Sie sich gern bei uns melden. 

Für kommenden Montag haben wir für das Discord-Treffen einen Server erstellt: https://discord.gg/5Bf4Gza

Mit freundlichen Grüßen

Hendrik Schutter und Team TextureSync




