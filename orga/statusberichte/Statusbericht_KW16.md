
#Statusbericht KW16 - TextureSync

Sehr geehrter Herr Nikolaropoulos,

hier der Statusbericht der vergangenen Woche TextureSync.

#Vergangene Arbeitswoche

##Grobdesign
Das Grobdesign wurde von Lukas Fürderer auf Grundlage einer Diskussion in der Gruppe erstellt.

##Mastertestplan
Der Mastertestplan wurde von Hendrik Schutter erstellt. 

##Netzwerkprotokoll
Robin Willmann ergänzte das Netzwerkprotokoll mit Fehlerhandhabungen, die bei der Erstellung des Mastertestplans auftraten. 

##Suche
Robin Willman erstellte ein Feindesign-Dokument für die Suche.

##Datenerhaltung
Lukas Fürderer erstellte ein Feindesign-Dokument für die Datenerhaltung des Servers.

##Mockups
Jannik Seiler dokumetierte die Mockups im Pflichenheft.

##3D-Preview Proof-of-Contept
Jannik Seiler portierte den in Java geschriebenen Code in Kotlin.

##UI Proof-of-Concept
Jannik Seiler erstellte eine UI-Demo in Kotlin um die UI-Elemente zu testen.

##Planung
In der Planung des Projektes wurden die erledigten Aufgaben aktuallisiert. 


#Nächste Arbeitswoche

##Feindesign fertigstellen

##Diagramme in Dokumente hinzuzufügen

##Alle Design Dokumente reviewen 

##Milestone End of Design 

#Aktuelle Informationen über das Projekt

##Website mit aktuellem Projektplan: https://planner.mosad.xyz/TextureSync.html

##Repository mit Code und Dokumenten: https://git.mosad.xyz/localhorst/TextureSync

Sollten Sie Fragen haben, können Sie sich gern bei uns melden. 


Mit freundlichen Grüßen  

Hendrik Schutter und Team TextureSync




