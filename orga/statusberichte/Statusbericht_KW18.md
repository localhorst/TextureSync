
#Statusbericht KW18 - TextureSync

Sehr geehrter Herr Nikolaropoulos,

hier der Statusbericht der vergangenen Woche TextureSync.

#Vergangene Arbeitswoche

##Server
* Lukas Fürderer hat das Persistence Module implementiert.
* Robin Willmann erstellte das Grundgerüst des kompletten Servers.
* Das Netzwerkprotrokoll wurde im Server implementiert.

##Client
* Jannik Seiler und Hendrik Schutter erstellten das Grundgerüst des Clients.

##Feindesign - UI-Elemente
* Jannik Seiler erstellte das Dokument. Hendrik Schutter ergänzte.

##Review alle Design Dokumente auf Grundlage der Rückmeldung vom 01.05.
* Beispiele zur Suche
* After-Date zur Suche hinzugefügt
* Client checkt Query explizit
* Suchergrbnis bei leerem Query ergänzt
* Fehlermeldungen im Pflichtenheft ergänzt
* Doppelte Bestätigung beim Löschen im Pflichtenheft beschrieben
* Import im Pflichtenheft beschrieben
* Mehrfachaktionen gelöscht

##Planung
In der Planung des Projektes wurden die erledigten Aufgaben aktualisiert. 

#Nächste Arbeitswoche

* Feindesign fertigstellen (UI Elemente)
* Milestone End of Design 
* Suche auf dem Server implementieren
* MainView des Clients implementieren

#Aktuelle Informationen über das Projekt

* Website mit aktuellem Projektplan: https://planner.mosad.xyz/TextureSync.html
* Repository mit Code und Dokumenten: https://git.mosad.xyz/localhorst/TextureSync

Sollten Sie Fragen haben, können Sie sich gern bei uns melden. 


Mit freundlichen Grüßen

Hendrik Schutter und Team TextureSync




