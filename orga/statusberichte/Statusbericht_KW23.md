
#Statusbericht KW23 - TextureSync

Sehr geehrter Herr Nikolaropoulos,

hier der Statusbericht der vergangenen Woche TextureSync.

#Vergangene Arbeitswoche

* Robin Willmann erstelle die Netzwerk-Schnittstelle für den Client
* Jannik Seiler und Hendrik Schutter erstellten die Logik des Clients
* Lukas Fürderer erstellt einen Demo-Datensatz mit 1000 Texturen welche die Metadaten auf dem Bild darstellen
* Robin Willmann erstellt das Wunschkriterium "Autoconnect" welches den Server automatisch findet

#Nächste Arbeitswoche

* Texturen ändern und Änderungen an den Server schicken
* Alle Texturen beim Start anzeigen
* Gefunden Texturen sofort anzeigen, nicht erst wenn alle gefunden sind
* UI Style der MainView anpassen
* Erste gefundene Textur initial ausgewählt
* Beginn der Test des Clients und der Server-Client Kommunikation
* Erstellen der Kurzanleitung

#Aktuelle Informationen über das Projekt

* Website mit aktuellem Projektplan: https://planner.mosad.xyz/TextureSync.html
* Repository mit Code und Dokumenten: https://git.mosad.xyz/localhorst/TextureSync

Sollten Sie Fragen haben, können Sie sich gern bei uns melden. 

Mit freundlichen Grüßen

Hendrik Schutter und Team TextureSync




