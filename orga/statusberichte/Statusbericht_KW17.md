
#Statusbericht KW17 - TextureSync

Sehr geehrter Herr Nikolaropoulos,

hier der Statusbericht der vergangenen Woche TextureSync.

#Vergangene Arbeitswoche

##Feindesign
Das Feindesign zur Datenerhaltung wurde von Lukas Fürderer erweitert.

##Mastertestplan
Der Mastertestplan wurde von Hendrik Schutter vervollständigt. 

##Netzwerkprotokoll
Robin Willmann ergänzte das Netzwerkprotokoll. 

##Impemeltieren
Robin Willmann erstellte das Grundgerüst des Servers.

##Planung
In der Planung des Projektes wurden die erledigten Aufgaben aktuallisiert. 


#Nächste Arbeitswoche

##Feindesign fertigstellen (UI Elemente)

##Alle Design Dokumente reviewen 

##Milestone End of Design 

###Durch die Osterfeiertage sowie der Abwesenheit von Jannik Seiler (auf dem RoboCup in Portugal mit Herrn Prof. Dorer) wurden nicht alle Ziele ereicht und werden deshalb in die nächste Woche übertragen.

#Aktuelle Informationen über das Projekt

##Website mit aktuellem Projektplan: https://planner.mosad.xyz/TextureSync.html

##Repository mit Code und Dokumenten: https://git.mosad.xyz/localhorst/TextureSync

Sollten Sie Fragen haben, können Sie sich gern bei uns melden. 


Mit freundlichen Grüßen  

Hendrik Schutter und Team TextureSync




