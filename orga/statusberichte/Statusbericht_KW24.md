
#Statusbericht KW24 - TextureSync

Sehr geehrter Herr Nikolaropoulos,

hier der Statusbericht der vergangenen Woche TextureSync.

#Vergangene Arbeitswoche

* Der Client wurde fertiggestellt und um mehrere Wunschfunktionen ergänzt. 
* Es wurden zwei Releases erstellt und getestet.
* Es wurden Kurzanleitungen für Server und Client erstellt.

#Nächste Arbeitswoche

* Die Präsentation für den 21.06.2019 fertigstellen.

#Aktuelle Informationen über das Projekt

Die Release 1.0.1 ist unsere erste Stable Release.

Alle Dokumente sind auf Moodle hochgeladen und somit von uns offiziell abgegeben. 

Sie können sich gerne unsere Kurzanleitungen anschauen um einen ersten Eindruck der Software zu bekommen:

https://git.mosad.xyz/localhorst/TextureSync/wiki

Die eingebetteteten YouTube-Clips sind unsere Demos.

* Website mit aktuellem Projektplan: https://planner.mosad.xyz/TextureSync.html
* Repository mit Code und Dokumenten: https://git.mosad.xyz/localhorst/TextureSync

Sollten Sie Fragen haben, können Sie sich gern bei uns melden. 
Wir freuen uns auf nächste Woche Freitag um Ihnen die fertige Software präsentieren zu können.

Mit freundlichen Grüßen

Hendrik Schutter und Team TextureSync




