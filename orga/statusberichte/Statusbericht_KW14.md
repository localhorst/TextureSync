
#Statusbericht KW14 - TextureSync

Sehr geehrter Herr Nikolaropoulos,

hier der Statusbericht der vergangenen Woche TextureSync.

#Vergangene Arbeitswoche

##Risikoanalyse
Die Risikoanalyse wurde von Robin Willmann und Hendrik Schutter vervollständigt und die Risiken und deren Gegenmaßnahmen wurden im Team besprochen und ggfs. angepasst.

##Pflichtenheft
Das Pflichtenheft wurde von Lukas Fürderer und Jannik Seiler auf der Grundlage des Lastenhefts und der Risikoanalyse erstellt. In einem Team-Meeting wurde über die Ergebnisse und die Änderungen gesprochen. 

##Proof of Concept

###Netzwerkprotokoll
Robin Willmann definierte ein Netzwerkprotokoll für die Verbindung des Servers und der Clients. 

###3D Preview
Hendrik Schutter erstellte eine Demo-Anwendung für eine 3D Preview einer Textur auf einem Quader.

##Planung
In der Planung des Projektes wurden die erledigten Aufgaben aktualisiert. 

#Nächste Arbeitswoche

##Pflichtenheft fertigstellen. 

##Architektur im Team festlegen.

## 3D Preview auf Kotlin/TornadoFx porten.

## Feindesign

## Mastertestplan

#Aktuelle Informationen über das Projekt

##Website mit aktuellem Projektplan: https://planner.mosad.xyz/TextureSync.html

##Repository mit Code und Dokumenten: https://git.mosad.xyz/localhorst/TextureSync

Sollten Sie Fragen haben, können Sie sich gern bei uns melden. 


Mit freundlichen Grüßen  

Hendrik Schutter und Team TextureSync




