
#Statusbericht KW20 - TextureSync

Sehr geehrter Herr Nikolaropoulos,

hier der Statusbericht der vergangenen Woche TextureSync.

#Vergangene Arbeitswoche

##Server
* Robin Willmann überarbeitete die Locking-Mechanismen.

##Client
* Jannik Seiler designte ein Modell für die Daten des Netzwerk-Controller und der UI.

##Review alle Design Dokumente auf Grundlage der Review am 10.05
* Feindesign der UI-Elemnte um das Kontextmenü für das Exportieren der Bilder ergänzt.
* Das Lastenheft wurde um eine Funktionalität ergänzt.
* Im Pflichtenheft wurden das Einfügedatum in MK#5 und die Fehlermeldungen hinzugefügt.

##Planung
In der Planung des Projektes wurden die erledigten Aufgaben aktualisiert. 

#Nächste Arbeitswoche
* Server Test vervollständigen
* Client Netzwerkschnittstelle

#Aktuelle Informationen über das Projekt

* Website mit aktuellem Projektplan: https://planner.mosad.xyz/TextureSync.html
* Repository mit Code und Dokumenten: https://git.mosad.xyz/localhorst/TextureSync

Sollten Sie Fragen haben, können Sie sich gern bei uns melden. 


Mit freundlichen Grüßen

Hendrik Schutter und Team TextureSync




